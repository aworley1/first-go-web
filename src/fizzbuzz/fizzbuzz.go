package main

import (
	"fmt"
	"strconv"
)

func main() {
	for i := 1; i <= 200; i++ {
		fmt.Println(fizzbuzz(i))
	}
}

func fizzbuzz(i int) string {
	if i % 5 == 0 && i % 3 == 0 {
		return "fizzbuzz"
	}
	if i % 3 == 0 {
		return "fizz"
	}
	if i % 5 == 0 {
		return "buzz"
	}
	return strconv.Itoa(i)
}